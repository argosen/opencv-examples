#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

int main( )
{
    cv::Mat img = cv::Mat::zeros(800, 600, CV_8UC3);

    cv::resize(img, img, cv::Size(800, 600));

    std::cout << "Image size = [" << img.cols << ", " << img.rows << "]" << std::endl;

    return 0;
}



