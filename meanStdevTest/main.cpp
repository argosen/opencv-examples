#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

int main( )
{
    // Generate data
    cv::Mat data(7,2,CV_32F);
    data.at<float>(0,0) = 1;
    data.at<float>(0,1) = 2;
    data.at<float>(1,0) = 3;
    data.at<float>(1,1) = 4;
    data.at<float>(2,0) = 5;
    data.at<float>(2,1) = 6;
    data.at<float>(3,0) = 7;
    data.at<float>(3,1) = 8;
    data.at<float>(4,0) = 9;
    data.at<float>(4,1) = 10;
    data.at<float>(5,0) = 11;
    data.at<float>(5,1) = 12;
    data.at<float>(6,0) = 13;
    data.at<float>(6,1) = 14;

    std::cout << data << std::endl;

    std::cout << "---" << std::endl;

    {
        cv::Scalar meanValue, stdValue;
        cv::meanStdDev(data, meanValue, stdValue);

        std::cout << "Mean: " << meanValue[0] << ", StDev: " << stdValue[0] << std::endl;
    }
    std::cout << "---" << std::endl;

    {
        unsigned int cols = data.cols;
        cv::Scalar meanValue, stdValue;
        for (unsigned int k=0; k<cols; ++k)
        {
            cv::meanStdDev(data.col(k), meanValue, stdValue);
            std::cout << "Mean: " << meanValue[0] << ", StDev: " << stdValue[0] << std::endl;
        }
    }

    std::cout << "---" << std::endl;

    cv::Mat data2(21,2,CV_32F);
    data2.at<float>(0,0) = 514.32617;
    data2.at<float>(1,0) = 504.49301;
    data2.at<float>(2,0) = 501.21533;
    data2.at<float>(3,0) = 499.57648;
    data2.at<float>(4,0) = 498.59314;
    data2.at<float>(5,0) = 497.93762;
    data2.at<float>(6,0) = 497.46936;
    data2.at<float>(7,0) = 497.11816;
    data2.at<float>(8,0) = 496.84503;
    data2.at<float>(9,0) = 496.62653;
    data2.at<float>(10,0) = 495.57846;
    data2.at<float>(11,0) = 494.70508;
    data2.at<float>(12,0) = 493.96606;
    data2.at<float>(13,0) = 493.33258;
    data2.at<float>(14,0) = 493.33258;
    data2.at<float>(15,0) = 493.33258;
    data2.at<float>(16,0) = 510.69452;
    data2.at<float>(17,0) = 510.69452;
    data2.at<float>(18,0) = 510.69452;
    data2.at<float>(19,0) = 532.45551;
    data2.at<float>(20,0) = 539.70917;

    data2.at<float>(0,1) = 0.32617;
    data2.at<float>(1,1) = 504.49301;
    data2.at<float>(2,1) = 501.21533;
    data2.at<float>(3,1) = 499.57648;
    data2.at<float>(4,1) = 498.59314;
    data2.at<float>(5,1) = 497.93762;
    data2.at<float>(6,1) = 497.46936;
    data2.at<float>(7,1) = 497.11816;
    data2.at<float>(8,1) = 496.84503;
    data2.at<float>(9,1) = 496.62653;
    data2.at<float>(10,1) = 495.57846;
    data2.at<float>(11,1) = 494.70508;
    data2.at<float>(12,1) = 493.96606;
    data2.at<float>(13,1) = 493.33258;
    data2.at<float>(14,1) = 493.33258;
    data2.at<float>(15,1) = 493.33258;
    data2.at<float>(16,1) = 510.69452;
    data2.at<float>(17,1) = 510.69452;
    data2.at<float>(18,1) = 510.69452;
    data2.at<float>(19,1) = 532.45551;
    data2.at<float>(20,1) = 539.70917;

    {
        unsigned int cols = data2.cols;
        cv::Scalar meanValue, stdValue;
        for (unsigned int k=0; k<cols; ++k)
        {
            cv::meanStdDev(data2.col(k), meanValue, stdValue);
            std::cout << "Mean2: " << meanValue[0] << ", StDev2: " << stdValue[0] << std::endl;
        }
    }

    std::cout << "---" << std::endl;


    return 0;
}



