#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>


// Compute barycentric coordinates (u, v, w) for
// point p with respect to triangle (a, b, c)
void fromCartesianToBarycentric(const cv::Point2f p, const cv::Point2f a, const cv::Point2f b, const cv::Point2f c, float &u, float &v, float &w)
{
    cv::Point v0 = b - a;
    cv::Point v1 = c - a;
    cv::Point v2 = p - a;
    float d00 = (v0.x * v0.x)+(v0.y * v0.y); //dotProduct(v0, v0);
    float d01 = (v0.x * v1.x)+(v0.y * v1.y); //dotProduct(v0, v1);
    float d11 = (v1.x * v1.x)+(v1.y * v1.y); //dotProduct(v1, v1);
    float d20 = (v2.x * v0.x)+(v2.y * v0.y); //dotProduct(v2, v0);
    float d21 = (v2.x * v1.x)+(v2.y * v1.y); //dotProduct(v2, v1);
    float denom = d00 * d11 - d01 * d01;
    v = (d11 * d20 - d01 * d21) / denom;
    w = (d00 * d21 - d01 * d20) / denom;
    u = 1.0f - v - w;
}

void fromBarycentricToCartesian(cv::Point2f& p, const cv::Point2f a, const cv::Point2f b, const cv::Point2f c, const float &u, const float &v, const float &w)
{
    p.x = u*a.x + v*b.x + w*c.x;
    p.y = u*a.y + v*b.y + w*c.y;
}

int main( )
{
    // Triangle 1
    cv::Point2f t1a(0.f,0.f);
    cv::Point2f t1b(100.f,0.f);
    cv::Point2f t1c(0.f,100.f);
    // Triangle 2
//    cv::Point2f t2a(0.f,0.f);
//    cv::Point2f t2b(200.f,0.f);
//    cv::Point2f t2c(0.f,200.f);
    cv::Point2f t2a(0.f,150.f);
    cv::Point2f t2b(300.f,0.f);
    cv::Point2f t2c(300.f,300.f);

    float u, v, w;
    cv::Point2f p1(25.f,25.f);
    cv::Point2f p2;
    fromCartesianToBarycentric( p1, t1a, t1b, t1c, u, v, w );
    fromBarycentricToCartesian( p2, t2a, t2b, t2c, u, v, w );

    std::cout << "Barycentric data: [" << u << ", " << v << ", " << w << "]" << std::endl;
    std::cout << "Final point: [" << p1.x << ", " << p1.y << "]=>[" << p2.x << ", " << p2.y << "]" << std::endl;

    // Show the result into an image
    float verticalSize = 300;
    float horizontalSize = 300;
    float verticalMargin = 20;
    float horizontalMargin = 20;
    cv::Point2f offset(horizontalMargin, verticalMargin);
    cv::Mat img = cv::Mat::zeros(verticalSize+verticalMargin+verticalMargin, horizontalSize+horizontalMargin+horizontalMargin, CV_8UC3);
    // Show the image rectangle
    cv::line(img, cv::Point2i(horizontalMargin, verticalMargin), cv::Point2i(horizontalMargin+horizontalSize, verticalMargin), cv::Scalar(255,255,255));
    cv::line(img, cv::Point2i(horizontalMargin, verticalMargin), cv::Point2i(horizontalMargin, verticalMargin+verticalSize), cv::Scalar(255,255,255));
    cv::line(img, cv::Point2i(horizontalMargin, verticalMargin+verticalSize), cv::Point2i(horizontalMargin+horizontalSize, verticalMargin+verticalSize), cv::Scalar(255,255,255));
    cv::line(img, cv::Point2i(horizontalMargin+horizontalSize, verticalMargin), cv::Point2i(horizontalMargin+horizontalSize, verticalMargin+verticalSize), cv::Scalar(255,255,255));

    // Show triangles
    cv::line(img, t1a+offset, t1b+offset, cv::Scalar(0,255,0));
    cv::line(img, t1a+offset, t1c+offset, cv::Scalar(0,255,0));
    cv::line(img, t1c+offset, t1b+offset, cv::Scalar(0,255,0));
    //  --
    cv::line(img, t2a+offset, t2b+offset, cv::Scalar(255,0,0));
    cv::line(img, t2a+offset, t2c+offset, cv::Scalar(255,0,0));
    cv::line(img, t2c+offset, t2b+offset, cv::Scalar(255,0,0));

    // Show the point
    cv::circle(img, p1+offset, 3, cv::Scalar(0,0,255), -1);
    cv::circle(img, p2+offset, 3, cv::Scalar(0,255,255), -1);

    cv::imshow("Final representation", img);
    cv::waitKey();

    return 0;
}



